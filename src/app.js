const express = require('express');

const app = express();

//Configuration Server
app.set('port', process.env.PORT || 3000);
const hostname = 'localhost';

//Middlewares
app.use(express.json());


//Routes

app.use(require('./routes/alumnos'));
app.use(require('./routes/profesores'));
app.use(require('./routes/salones'));
app.use(require('./routes/areasVerdes'));






app.listen(app.get('port'), hostname, () =>{
    console.log('Servidor activo en puerto '+ app.get('port'));
})