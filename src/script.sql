-- SCRIPT PARA LA BASE DE DATOS

-- Crear base de datos
CREATE DATABASE IF NOT EXISTS db_sistema_control;
USE db_sistema_control;

-- Tabla Personal Esmeralda
CREATE TABLE `db_sistema_control`.`tbl_personalesmeralda` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(45) NOT NULL,
  `Edad` INT(3) NULL,
  `Telefono` INT NULL,
  `Email` VARCHAR(45) NULL,
  `Activo` TINYINT NULL,
  `ID_Tipo` INT NULL,
  `ID_Materia` INT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- Tabla Materias 
CREATE TABLE `db_sistema_control`.`tbl_materia` (
  `ID_Materia` INT NOT NULL AUTO_INCREMENT,
  `Materia` VARCHAR(45) NULL,
  `Activo` TINYINT NULL,
  PRIMARY KEY (`ID_Materia`));

  -- Alteración de la base de datos


  -- Tabla Areas Verdes

  CREATE TABLE `db_sistema_control`.`tbl_areasVerdes` (
  `ID_AreaVerde` INT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(45) NULL,
  `Ubicacion` VARCHAR(60) NULL,
  PRIMARY KEY (`ID_AreaVerde`));


  -- Tabla Salones

    CREATE TABLE `db_sistema_control`.`tbl_salones` (
  `ID_Salon` INT NOT NULL AUTO_INCREMENT,
  `Codigo` VARCHAR(45) NULL,
  `Edificio` VARCHAR(60) NULL,
  PRIMARY KEY (`ID_Salon`));
