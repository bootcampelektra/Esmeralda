const express = require("express");
const router = express.Router();
const mysqlConnection = require("../database");

router.get("/profesor", (request, respuesta) => {
  mysqlConnection.query(
    "SELECT * FROM tbl_personalesmeralda where ID = 1;",
    (err, rows, fields) => {
      if (!err) {
        respuesta.json(rows);
      } else {
        console.error(err);
      }
    }
  );
});

router.put("/profesor/actualizar/:id", (request, respuesta) => {
  const id = request.params.id;
  const valores = request.body;

  let Nombre = valores.Nombre;
  let Edad = valores.Edad;
  let Telefono = valores.Telefono;
  let Email = valores.Email;
  let Activo = valores.Activo;
  let ID_Tipo = valores.ID_Tipo;
  let ID_Materia = valores.ID_Materia;


  // Validaciones
  if (!isNaN(id) === false || !id) {

    return respuesta.status(401).json({
      msg: "No puede ir el dato id vacío y debe ser númerico"
    });
  }

  if (!Nombre) {

    return respuesta.status(401).json({
      msg: "No puede ir el campo nombre vacío"
    });

  }


  if (!isNaN(Edad) === false || !Edad) {

    return respuesta.status(401).json({
      msg: "No puede ir el dato edad vacío y debe ser númerico"
    });
  }


  if (!isNaN(Telefono) === false || !Telefono) {

    return respuesta.status(401).json({
      msg: "No puede ir el dato teléfono vacío y debe ser númerico"
    });
  }

  if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(Email)) {
    return respuesta.status(401).json({
      msg: "Correo incorrecto"
    });
  }

  if (!isNaN(Activo) === false || !Activo) {

    return respuesta.status(401).json({
      msg: "No puede ir el dato activo vacío y debe ser númerico"
    });
  }

  if (!isNaN(ID_Tipo) === false || !ID_Tipo) {

    return respuesta.status(401).json({
      msg: "No puede ir el dato ID_Tipo vacío y debe ser númerico"
    });
  }


  if (!isNaN(ID_Materia) === false || !ID_Materia) {

    return respuesta.status(401).json({
      msg: "No puede ir el dato ID_Materia vacío y debe ser númerico"
    });
  }


  mysqlConnection.query(
    "UPDATE tbl_personalesmeralda SET Nombre = ? ,Edad = ? ,Telefono = ? ,Email = ? , Activo  = ? ,ID_Tipo = ? , ID_Materia = ? WHERE ID = ? ;",
    [Nombre, Edad, Telefono, Email, Activo, ID_Tipo, ID_Materia, id],
    (err, rows, fields) => {
      if (!err) {
        respuesta.json({
          msg: "Profesor modificado"
        });
      } else {
        console.log(err);
      }
    }
  );
});

module.exports = router;