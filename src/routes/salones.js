const {
    response
} = require('express');
const express = require('express');
const {
    request
} = require('http');
const router = express.Router();

const mysqlConnection = require('../database');

//Guadar salones
router.post('/api/gSalon', (req, res) => {
    const {
        codigo,
        edificio,
        idsalon
    } = req.body;

    // Validaciones

    if (!codigo) {

        return res.status(401).json({
            msg: "No puede ir el campo código vacío"
        });
    }

    if (!edificio) {

        return res.status(401).json({
            msg: "No puede ir el campo edificio vacío"
        });

    }

    if (!isNaN(idsalon) === false || !idsalon) {
        
        return res.status(401).json({
        msg: "No puede ir el dato idSalon vacío y debe ser númerico"
    });
    }

    mysqlConnection.query(`INSERT INTO tbl_salones(ID_Salon,Codigo,Edificio) VALUES ( ${idsalon},"${codigo}", "${edificio}");`, (error, rows, fields) => {
        if (!error) {
            res.json({
                msg: "Salón agregado correctamente"
            })
        } else {
            res.json(error)
        }

    })
});



//Buscar salones
router.get('/api/salones', (resquest, response) => {
    mysqlConnection.query('SELECT * FROM db_sistema_control.tbl_salones', (err, rows, fields) => {
        if (!err) {
            response.json(rows)
        } else {
            console.log(err);
        }
    });

})

//Actualizar Usuario
router.put('/api/actSalon/:id', (req, res) => {

    const id = req.params.id;
    const codigo = req.body.codigo;
    const edificio = req.body.edificio;


    
    // Validaciones

    if (!codigo) {

        return res.status(401).json({
            msg: "No puede ir el campo código vacío"
        });
    }

    if (!edificio) {

        return res.status(401).json({
            msg: "No puede ir el campo edificio vacío"
        });

    }

    if (!isNaN(id) === false || !id) {
        
        return res.status(401).json({
        msg: "No puede ir el dato idSalon vacío y debe ser númerico"
    });
    }

    mysqlConnection.query(
        "UPDATE tbl_salones SET Codigo= ?, Edificio= ? WHERE ID_Salon= ?",
        [codigo, edificio, id],
        (err, rows, fields) => {

            if (!err) {
                res.json({
                    msg: "Salón modificado correctamente"
                });

            } else {
                res.json(err);
            }
        })
});

//Eliminar Usuario
router.delete('/api/dSalon', (req, res) => {
    const id = req.body.idsalon;

    // Validaciones
    if (!isNaN(id) === false || !id) {
        
        return res.status(401).json({
        msg: "No puede ir el dato idSalon vacío y debe ser númerico"
    });
    }

    mysqlConnection.query("DELETE FROM `db_sistema_control`.`tbl_salones` WHERE `ID_Salon`=" + id, (error, rows, fields) => {
        if (!error) {
            res.json({
                msg: "Salón Eliminado correctamente"
            })
        } else {
            res.json(error)
        }

    })
});


module.exports = router;