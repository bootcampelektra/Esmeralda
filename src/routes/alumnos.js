const express = require("express");
const router = express.Router();

const mysqlConnection = require("../database");

router.post('/guardaralumno', (request, respuesta)=>{
    const alumnos = request.body;
    let ID = alumnos.ID;
    let Nombre = alumnos.Nombre;
    let Edad = alumnos.Edad;
    let Telefono = alumnos.Telefono;
    let Email = alumnos.Email;
    let Activo = alumnos.Activo;
    let ID_Tipo = alumnos.ID_tipo;
    let ID_Materia = alumnos.IDmateria

    mysqlConnection.query("INSERT INTO db_sistema_control.tbl_personalesmeralda (ID, Nombre, Edad, Telefono, Email, Activo, ID_Tipo, ID_Materia) VALUES (?, ?,? ,? ,? ,? ,? ,? );",[ID, Nombre, Edad, Telefono, Email, Activo, ID_Tipo, ID_Materia],
        (err, rows, fields)=>{
            if (!err) {
                respuesta.json(rows);
            }else{
                console.log(err)
            }   
        })

});





router.put("/alumno/actualizar/:id", (request, respuesta) => {
    const id = request.params.id;
    const valores = request.body;
  
    let Nombre = valores.Nombre;
    let Edad = valores.Edad;
    let Telefono = valores.Telefono;
    let Email = valores.Email;
    let Activo = valores.Activo;
    let ID_Tipo = valores.ID_Tipo;
    let ID_Materia = valores.ID_Materia;
  
  
    // Validaciones
    if (!isNaN(id) === false || !id) {
  
      return respuesta.status(401).json({
        msg: "No puede ir el dato id vacío y debe ser númerico"
      });
    }
  
    if (!Nombre) {
  
      return respuesta.status(401).json({
        msg: "No puede ir el campo nombre vacío"
      });
  
    }
  
  
    if (!isNaN(Edad) === false || !Edad) {
  
      return respuesta.status(401).json({
        msg: "No puede ir el dato edad vacío y debe ser númerico"
      });
    }
  
  
    if (!isNaN(Telefono) === false || !Telefono) {
  
      return respuesta.status(401).json({
        msg: "No puede ir el dato teléfono vacío y debe ser númerico"
      });
    }
  
    if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(Email)) {
      return respuesta.status(401).json({
        msg: "Correo incorrecto"
      });
    }
  
    if (!isNaN(Activo) === false || !Activo) {
  
      return respuesta.status(401).json({
        msg: "No puede ir el dato activo vacío y debe ser númerico"
      });
    }
  
    if (!isNaN(ID_Tipo) === false || !ID_Tipo) {
  
      return respuesta.status(401).json({
        msg: "No puede ir el dato ID_Tipo vacío y debe ser númerico"
      });
    }
  
  
    if (!isNaN(ID_Materia) === false || !ID_Materia) {
  
      return respuesta.status(401).json({
        msg: "No puede ir el dato ID_Materia vacío y debe ser númerico"
      });
    }
  
  
    mysqlConnection.query(
      "UPDATE tbl_personalesmeralda SET Nombre = ? ,Edad = ? ,Telefono = ? ,Email = ? , Activo  = ? ,ID_Tipo = ? , ID_Materia = ? WHERE ID = ? ;",
      [Nombre, Edad, Telefono, Email, Activo, ID_Tipo, ID_Materia, id],
      (err, rows, fields) => {
        if (!err) {
          respuesta.json({
            msg: "Alumno modificado"
          });
        } else {
          console.log(err);
        }
      }
    );
  });


   
   module.exports = router;
