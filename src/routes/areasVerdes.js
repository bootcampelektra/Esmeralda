const {
  request,
  response
} = require("express");
const express = require("express");
const router = express.Router();
const cors = require('cors');


const mysqlConnection = require("../database");
router.use(cors());

//GET

router.get("/api/todasAreasVerdes", (resquest, response) => {
  mysqlConnection.query(" SELECT * FROM db_sistema_control.tbl_areasVerdes",
    (err, rows, fields) => {
      if (!err) {
        response.json(rows);
      } else {
        console.log(err);
      }
    });
});



router.get("/api/solaAreaVerde/:id", (request, response) => {
  const id = request.params.id;

  // Validaciones 

  if (!isNaN(id) === false || !id) {
        
    return response.status(401).json({
    msg: "No puede ir el dato id vacío y debe ser númerico"
});
}

  mysqlConnection.query(
    "SELECT * FROM tbl_areasVerdes WHERE ID_AreaVerde = ?;",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json(rows);
      } else {}
    }
  );
});


//POST
router.post("/api/guardarAreaVerde", async (request, response) => {
  const nombre = request.body.Nombre;
  const Ubicacion = request.body.Ubicacion;


  if (!nombre) {

    return response.status(401).json({
        msg: "No puede ir el campo nombre vacío"
    });
}

if (!Ubicacion) {

    return response.status(401).json({
        msg: "No puede ir el campo ubicación vacío"
    });

}


   mysqlConnection.query(
     "INSERT INTO tbl_areasVerdes (Nombre, Ubicacion) VALUES (?,?);",
     [nombre, Ubicacion],
     (err, rows, fields) => {
       if (!err) {
        
        
         response.json({
          msg: "Area Verde Guardada"
         })

       } else {
         response.json(err);
       }
     }
   ); 

 
  



});

//PUT

router.put("/api/modificarAreaVerde/:id", (request, response) => {
  const id = request.params.id;
  const nombre = request.body.Nombre;
  const Ubicacion = request.body.Ubicacion;



  // Validaciones

  if (!nombre) {

    return response.status(401).json({
        msg: "No puede ir el campo nombre vacío"
    });
}

if (!Ubicacion) {

    return response.status(401).json({
        msg: "No puede ir el campo ubicación vacío"
    });

}

if (!isNaN(id) === false || !id) {
        
  return response.status(401).json({
  msg: "No puede ir el dato id vacío y debe ser númerico"
});
}


  mysqlConnection.query(
    "UPDATE tbl_areasverdes SET Nombre = ?, Ubicacion = ?  WHERE ID_AreaVerde = ?",
    [nombre, Ubicacion, id],
    (err, rows, fields) => {
      if (!err) {
        response.json({
          msg: "Area verde modificada correctamente"
        });
      } else {
        response.json(err);
      }
    }
  );


});


//DELETE

router.delete("/api/eliminarAreaVerde/:id", (request, response) => {
  const id = request.params.id;

  // Validaciones

  if (!isNaN(id) === false || !id) {
        
    return response.status(401).json({
    msg: "No puede ir el dato id vacío y debe ser númerico"
  });
  }

  mysqlConnection.query(
    "DELETE FROM tbl_areasVerdes WHERE ID_AreaVerde  = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json({
          msg: "Area Verde eliminada correctamente"
        })
      } else {
        response.json(err);
      }
    }
  );
});

module.exports = router;